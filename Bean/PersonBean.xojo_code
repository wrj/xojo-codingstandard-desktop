#tag Class
Protected Class PersonBean
	#tag Method, Flags = &h0
		Function Delete(Id As String) As Boolean
		  Dim d As New Model.Person
		  return d.Delete(Id)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DeleteWS(Id As String) As Boolean
		  dim c as new CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim data() As String
		  
		  
		  
		  'System.DebugLog(formData.ToString)
		  
		  c.OptionVerbose=true
		  c.OptionHeader = false
		  'c.OptionPost = True
		  c.CollectDebugData = True
		  c.CollectOutputData = true
		  data.Append("Accept-Version: 1.0")
		  'data.Append("Content-Type: application/json")
		  'data.Append("Content-Length: " + Str(LenB(Id.ToString)))
		  c.SetOptionHTTPHeader(data)
		  c.OptionCustomRequest = "DELETE"
		  
		  'c.OptionPostFields = Id.ToString
		  c.OptionURL = App.kURLService + "/persons/"+Id
		  response = c.Perform
		  if response = 0 Then
		    rawData = New JSONItem(c.OutputData)
		    if rawData.Value("status") then
		      Return True
		    End
		  End
		  Return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetById(id As String) As JSONItem
		  Dim personInstance As new Model.Person
		  personInstance.GetByID(Id)
		  
		  Dim data As New JSONItem
		  
		  data.Value("firstname") = personInstance.FirstName
		  data.Value("lastname") = personInstance.LastName
		  data.Value("occupation") = personInstance.Occupation
		  data.Value("dob") = personInstance.DOB
		  data.Value("email") = personInstance.Email
		  return data
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetByIDWS(id As String) As JSONItem
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim data() As String
		  
		  c.OptionVerbose=true
		  c.OptionURL = App.kURLService + "/persons/" + id
		  c.OptionHeader = False
		  data.Append("Accept-Version: 1.0")
		  data.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(data)
		  c.CollectDebugData = True
		  c.CollectOutputData = true
		  response = c.Perform
		  Dim reqData As New JSONItem(c.OutputData)
		  
		  Dim dataR As New JSONItem
		  dataR.Value("firstname") = reqData.Value("firstname")
		  dataR.Value("lastname") = reqData.Value("lastname")
		  dataR.Value("occupation") = reqData.Value("occupation")
		  dataR.Value("dob") = reqData.Value("dateOfBirth")
		  dataR.Value("email") = reqData.Value("email")
		  
		  return dataR
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAll() As RecordSet
		  Dim personInstance As new Model.Person
		  return personInstance.ListAll
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAllWS() As String
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim data() As String
		  
		  c.OptionVerbose=true
		  c.OptionURL = App.kURLService + "/persons/list/"
		  c.OptionHeader = False
		  data.Append("Accept-Version: 1.0")
		  data.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(data)
		  c.CollectDebugData = True
		  c.CollectOutputData = true
		  response = c.Perform
		  if response = 0 Then
		    return c.OutputData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Save(FormData As JSONItem) As Boolean
		  Dim p As New Model.Person
		  return p.Save(FormData)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SaveWS(formData As JSONItem) As Boolean
		  dim c as new CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim data() As String
		  
		  formData.Value("dateOfBirth") = Utils.gDataToStringForJSON(formData.Value("dateOfBirth"))
		  Dim emJS As New JSONItem
		  emJS = formData.Value("email")
		  
		  Dim emIt As New JSONItem
		  
		  For i As Integer = 0 To emJS.Count - 1 ' loop over data
		    Dim iItem As JSONItem = emJS.Child(i)
		    emIt.Append(iItem.Value("name"))
		  Next
		  
		  Dim nEm As New JSONItem
		  nEm.Value("new") = emIt
		  formData.Value("email") = nEm.ToString
		  
		  c.OptionVerbose=true
		  c.OptionHeader = false
		  c.OptionPost = True
		  c.CollectDebugData = True
		  c.CollectOutputData = true
		  data.Append("Accept-Version: 1.0")
		  data.Append("Content-Type: application/json")
		  data.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  c.SetOptionHTTPHeader(data)
		  
		  c.OptionPostFields = formData.ToString
		  c.OptionURL = App.kURLService + "/persons"
		  response = c.Perform
		  if response = 0 Then
		    rawData = New JSONItem(c.OutputData)
		    if rawData.Value("status") then
		      Return True
		    End
		  End
		  Return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetProperty()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Update(FormData As JSONItem) As Boolean
		  Dim p As New Model.Person
		  return p.Update(FormData)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateWS(formData As JSONItem) As Boolean
		  dim c as new CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim data() As String
		  
		  formData.Value("dateOfBirth") = Utils.gDataToStringForJSON(formData.Value("dateOfBirth"))
		  Dim emJS As New JSONItem
		  emJS = formData.Value("email")
		  
		  Dim jsonEmail As new JSONItem
		  Dim emIt As New JSONItem
		  Dim emItUpdate As New JSONItem
		  Dim emItDelete As new JSONItem
		  
		  For i As Integer = 0 To emJS.Count - 1 ' loop over data
		    Dim iItem As JSONItem = emJS.Child(i)
		    if  iItem.Value("id") = "" Then
		      emIt.Append(iItem.Value("name"))
		    Else
		      emItUpdate.Append(iItem)
		    end
		  Next
		  
		  emItDelete = formData.Value("emailDelete")
		  
		  if emIt.Count > 0 Then
		    jsonEmail.Value("new") = emIt
		  end
		  
		  jsonEmail.Value("update") = emItUpdate
		  
		  if emItDelete.Count > 0 Then
		    jsonEmail.Value("delete") = emItDelete
		  end
		  
		  
		  
		  formData.Value("email") = jsonEmail.ToString
		  'System.DebugLog(formData.ToString)
		  
		  c.OptionVerbose=true
		  c.OptionHeader = false
		  'c.OptionPost = True
		  c.CollectDebugData = True
		  c.CollectOutputData = true
		  data.Append("Accept-Version: 1.0")
		  data.Append("Content-Type: application/json")
		  data.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  c.SetOptionHTTPHeader(data)
		  c.OptionCustomRequest = "PUT"
		  
		  c.OptionPostFields = formData.ToString
		  c.OptionURL = App.kURLService + "/persons/"+formData.Value("id")
		  response = c.Perform
		  if response = 0 Then
		    rawData = New JSONItem(c.OutputData)
		    if rawData.Value("status") then
		      Return True
		    End
		  End
		  Return false
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private Data As JSONItem
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
