#tag Interface
Protected Interface IQueryDatabase
	#tag Method, Flags = &h0
		Function Delete(id as String) As Boolean
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetByID(id as String)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAll() As RecordSet
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Save(formData as JSONItem) As Boolean
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetProperty(data As JSONItem)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Update(formData as JSONItem) As Boolean
		  
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Interface
#tag EndInterface
