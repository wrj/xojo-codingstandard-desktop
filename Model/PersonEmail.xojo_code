#tag Class
Protected Class PersonEmail
	#tag Method, Flags = &h0
		Function Delete(Id as String) As Boolean
		  If Not Utils.gDBConnect Then Return(False)
		  
		  Dim d As PostgreSQLPreparedStatement
		  d = App.DB.Prepare("DELETE FROM person_email WHERE id = $1")
		  d.SQLExecute(Id)
		  
		  If Not Utils.gDBTransactionError Then Return(False)
		  
		  Return True
		  
		  
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetById(Id as String)
		  If Not Utils.gDBConnect Then Return
		  App.DB.SQLExecute("BEGIN TRANSACTION")
		  
		  Dim g As PostgreSQLPreparedStatement
		  g = App.DB.Prepare("SELECT * FROM id WHERE Id = $1")
		  
		  Dim rs As RecordSet = g.SQLSelect(Id)
		  
		  
		  If Not Utils.gDBTransactionError Then Return
		  
		  If rs <> Nil Then
		    Do Until rs.EOF
		      
		      self.Id = rs.Field("id").StringValue
		      self.Email = rs.Field("email").StringValue
		      rs.MoveNext
		      
		    Loop
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetByPerson(Id As String) As RecordSet
		  If Not Utils.gDBConnect Then Return nil
		  App.DB.SQLExecute("BEGIN TRANSACTION")
		  
		  Dim g As PostgreSQLPreparedStatement
		  g = App.DB.Prepare("SELECT * FROM person_email WHERE person_id = $1")
		  
		  Dim rs As RecordSet = g.SQLSelect(Id)
		  
		  
		  If Not Utils.gDBTransactionError Then Return nil
		  
		  If rs <> Nil Then
		    return rs
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAll() As RecordSet
		  // If Not Utils.gDBConnect Then Return(nil)
		  // App.DB.SQLExecute("BEGIN TRANSACTION")
		  // 
		  // Dim l As PostgreSQLPreparedStatement
		  // l = App.DB.Prepare("SELECT * FROM person ORDER BY updated_date DESC")
		  // 
		  // Dim rs As RecordSet = l.SQLSelect()
		  // 
		  // If Not Utils.gDBTransactionError Then Return(nil)
		  // Return rs
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Save(Data As JSONItem) As Boolean
		  //บายข้อมูล
		  SetProperty(Data)
		  
		  //เชคคอนเนตชั่น
		  If Not Utils.gDBConnect Then Return(False)
		  
		  App.DB.SQLExecute("BEGIN TRANSACTION")
		  
		  MsgBox("Insert Email")
		  Dim i As PostgreSQLPreparedStatement
		  i = App.DB.Prepare("INSERT INTO person_email (name,person_id) VALUES ($1, $2)")
		  i.SQLExecute(self.Email, self.Person_id)
		  
		  If Not Utils.gDBTransactionError Then Return(False)
		  
		  Return True
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SaveWebService(FormData As JSONItem) As JSONItem
		  // Dim http As New Chilkat.Http // สร้างตัวแปร http data type Chillkat HTTP
		  // Dim authChilkat As Boolean // สร้างตัวแปร data type Boolean
		  // Dim response As Chilkat.HttpResponse // สร้างตัวแปร data type Chilkat.HttpResponse
		  // Dim strReturn As New JSONItem // สร้างตัวแปร data type JSONItem
		  // Dim rawData As JSONItem // สร้างตัวแปรที่ทำเป็น json
		  // 
		  // 
		  // //  Any string unlocks the component for the 1st 30-days.
		  // authChilkat = http.UnlockComponent(unlockChilkat)
		  // 
		  // // ตรวจสอบว่า  Code ที่ใส่เข้าไปเพื่อใช้ Plugin chilkate นี้ สามารถใช้งานได้หรือ
		  // If (authChilkat <> True) Then
		  // System.DebugLog(http.LastErrorText)
		  // strReturn.Value("status") = false
		  // strReturn.Value("message") = "Code Unlock Component ผิดพลาด"
		  // Else
		  // Dim request As New Chilkat.HttpRequest // สร้างตัวแปร request data type Chilkat.HttpRequest
		  // request.Charset = "UTF-8" // กำหนด Charset เป็น UTF 8
		  // 
		  // // เพิ่ม Parameter เข้าไปในตัวแปร request เพื่อที่จะส่งไปยัง Webservice โดยการเพิ่ม Parameter จะกำหนดเป็น Key, Value
		  // request.AddParam("firstName",formData.Value("firstName"))
		  // request.AddParam("lastName",formData.Value("lastName"))
		  // request.AddParam("email",formData.Value("email"))
		  // request.AddParam("occupation",formData.Value("occupation"))
		  // request.AddParam("dob",formData.Value("dob"))
		  // 
		  // // กำหนด Http Header ชื่อ Accept-version (เป็นข้อกำหนดทาง Webservice)
		  // http.SetRequestHeader("Accept-Version",Self.apiVersion)
		  // 
		  // // urlService = URL Webservice สำหรับส่ง Request
		  // response = http.PostUrlEncoded(urlService + "personal",request)
		  // 
		  // // ตรวจสอบ response
		  // If (response Is Nil ) Then
		  // System.DebugLog(http.LastErrorText)
		  // strReturn.Value("status") = false
		  // strReturn.Value("message") = "ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบการเชื่อมต่อของคุณ"
		  // Else
		  // rawData = New JSONItem(response.BodyStr.ToText)
		  // strReturn.Value("status") = rawData.Value("status")
		  // if rawData.Value("status") <> "true" then
		  // strReturn.Value("message") = rawData.Value("message")
		  // else
		  // strReturn.Value("message") = "เพิ่มข้อมูลสำเร็จ"
		  // end
		  // 
		  // End If
		  // End If
		  // Return strReturn
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetProperty(Data As JSONItem)
		  self.Id = Data.Value("id")
		  self.Email = Data.Value("name")
		  self.Person_id = Data.Value("person_id")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Update(Data as JSONItem) As Boolean
		  SetProperty(Data)
		  
		  If Not Utils.gDBConnect Then Return(False)
		  
		  Dim u As PostgreSQLPreparedStatement
		  u = App.DB.Prepare("UPDATE person_email SET name = $1 WHERE id = $2")
		  u.SQLExecute(Email, Id)
		  
		  If Not Utils.gDBTransactionError Then Return(False)
		  
		  Return True
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		Email As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Id As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Person_id As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="email"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="person_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
