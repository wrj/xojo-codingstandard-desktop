#tag Class
Protected Class Person
Implements IQueryDatabase
	#tag Method, Flags = &h0
		Function Delete(Id as String) As Boolean
		  If Not Utils.gDBConnect Then Return(False)
		  App.DB.SQLExecute("BEGIN TRANSACTION")
		  
		  Dim d As PostgreSQLPreparedStatement
		  d = App.DB.Prepare("DELETE FROM person WHERE person_id = $1")
		  d.SQLExecute(Id)
		  
		  If Not Utils.gDBTransactionError Then Return(False)
		  App.DB.Commit
		  Return True
		  
		  
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetByID(Id as String)
		  If Not Utils.gDBConnect Then Return
		  App.DB.SQLExecute("BEGIN TRANSACTION")
		  
		  Dim g As PostgreSQLPreparedStatement
		  g = App.DB.Prepare("SELECT * FROM person WHERE person_id = $1")
		  
		  Dim rs As RecordSet = g.SQLSelect(Id)
		  
		  
		  If Not Utils.gDBTransactionError Then Return
		  
		  If rs <> Nil Then
		    Do Until rs.EOF
		      
		      self.Id = rs.Field("person_id").StringValue
		      self.FirstName = rs.Field("firstname").StringValue
		      self.LastName = rs.Field("lastname").StringValue
		      self.Occupation = rs.Field("occupation").StringValue
		      self.DOB = rs.Field("date_of_birth")
		      // Email
		      Dim pe As New Model.PersonEmail
		      Dim emResult As New JSONItem
		      Dim rse As RecordSet = pe.GetByPerson(self.id)
		      Do Until rse.EOF
		        Dim peMail As New JSONItem
		        peMail.Value("id") = rse.Field("id").StringValue
		        peMail.Value("name") = rse.Field("name").StringValue
		        emResult.Append(peMail)
		        rse.MoveNext
		      Loop
		      self.Email = emResult
		      rs.MoveNext
		    Loop
		  End If
		  
		  
		  
		  
		  // MsgBox id.ToText+"inside"
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAll() As RecordSet
		  If Not Utils.gDBConnect Then Return(nil)
		  App.DB.SQLExecute("BEGIN TRANSACTION")
		  
		  Dim l As PostgreSQLPreparedStatement
		  l = App.DB.Prepare("SELECT * FROM person ORDER BY updated_date DESC")
		  
		  Dim rs As RecordSet = l.SQLSelect()
		  
		  If Not Utils.gDBTransactionError Then Return(nil)
		  
		  Return rs
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Save(data as JSONItem) As Boolean
		  //บายข้อมูล
		  SetProperty(data)
		  
		  //เชคคอนเนตชั่น
		  If Not Utils.gDBConnect Then Return(False)
		  App.DB.SQLExecute("BEGIN TRANSACTION")
		  
		  Dim i As PostgreSQLPreparedStatement
		  Dim rs As RecordSet
		  i = App.DB.Prepare("INSERT INTO person (firstname, lastname, occupation, date_of_birth) VALUES ($1, $2, $3, $4) returning person.person_id")
		  rs = i.SQLSelect(FirstName, LastName, Occupation, DOB)
		  
		  If Not Utils.gDBTransactionError Then Return(False)
		  
		  For j As Integer = 0 To self.Email.Count - 1 ' loop over data
		    Dim emailItem As JSONItem = self.Email.Child(j)
		    Dim pE As New Model.PersonEmail
		    emailItem.Value("person_id") = rs.IdxField(1).StringValue
		    If Not pE.Save(emailItem) Then Return(False)
		  Next
		  
		  
		  App.DB.Commit
		  Return True
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SaveWebService(formData As JSONItem) As JSONItem
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetProperty(data As JSONItem)
		  self.Id = data.Value("id")
		  self.FirstName = data.Value("firstname")
		  self.LastName = data.Value("lastname")
		  self.Occupation = data.Value("occupation")
		  self.DOB = data.Value("dateOfBirth")
		  self.Email = data.Value("email")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Update(Data as JSONItem) As Boolean
		  SetProperty(Data)
		  
		  If Not Utils.gDBConnect Then Return(False)
		  App.DB.SQLExecute("BEGIN TRANSACTION")
		  
		  Dim u As PostgreSQLPreparedStatement
		  u = App.DB.Prepare("UPDATE person SET firstname = $1, lastname = $2 , occupation = $3, date_of_birth = $4 WHERE person_id = $5")
		  u.SQLExecute(FirstName, LastName, Occupation, DOB , Id)
		  
		  If Not Utils.gDBTransactionError Then Return(False)
		  
		  // Add New and Update Email
		  for j as integer=0 to self.Email.Count-1
		    Dim pE As New Model.PersonEmail
		    Dim emailItem As JSONItem = self.Email.Child(j)
		    emailItem.Value("person_id") = Id
		    If emailItem.Value("id") = "" Then
		      If Not pE.Save(emailItem) Then Return(False)
		    Else
		      If Not pE.Update(emailItem) Then Return(False)
		    End If
		  Next
		  
		  
		  // Delete
		  Dim emailDelete As JSONItem = Data.Value("emailDelete")
		  if emailDelete.Count > 0 then
		    'MsgBox "มีอีเมลล์ที่จะลบ"
		    for k as integer=0 to emailDelete.Count -1
		      Dim pE As New Model.PersonEmail
		      Dim emailId As String = emailDelete.Value(k)
		      If Not pE.Delete(emailId) Then Return(False)
		    Next
		  else
		    MsgBox "ไม่มีอีเมลล์ที่จะลบ"
		  end
		  
		  
		  App.DB.Commit
		  Return True
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private ApiVersion As String
	#tag EndProperty

	#tag Property, Flags = &h0
		DOB As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		Email As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		FirstName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Id As String
	#tag EndProperty

	#tag Property, Flags = &h0
		LastName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Occupation As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="FirstName"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LastName"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Occupation"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
