#tag Module
Protected Module Utils
	#tag Method, Flags = &h0
		Function gCalculateAge(dob As String) As Integer
		  if dob.Len < 10 Then
		  else
		    Dim date as Date
		    date = gStringToDate(dob)
		    
		    Dim currentDate As new Date
		    
		    Return currentDate.Year - date.Year
		  end if 
		  
		  
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function gConvertStringDateToSQL(dateTxt As String) As String
		  Dim anArray(-1) as String
		  Dim re As String
		  anArray = Split(dateTxt,"/")
		  re = Str(val(anArray(2))) + "-" + Str(val(anArray(1))) + "-" + Str(val(anArray(0)))
		  
		  return re
		  
		  
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function gDataToStringForJSON(InputDate As Date) As String
		  Dim datestring As String
		  if InputDate <> Nil then
		    datestring = InputDate.Day.ToText+"/"+InputDate.Month.ToText+"/"+InputDate.Year.ToText
		  end if
		  return datestring
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function gDateDisplayFormat(date as String) As String
		  Dim months() as Text = Array("JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC")
		  Dim mtemp as String
		  Dim mresult as String
		  Dim j as Integer
		  Dim chk as String
		  'for each  s as Text in months
		  'MsgBox s
		  'Next
		  
		  if date = "" then
		    Return ""
		  Else
		    if date.len >=8 And date.len < 11 Then
		      mtemp = date.Mid(3,2)
		      j = Val(mtemp)-1
		      if j < 12 Then
		        mtemp = months(j)
		        mresult = date.Mid(1,2)+" "+mtemp+" "+date.Mid(5,4)
		        Return mresult
		      else
		        MsgBox "กรุณาตรวจสอบหมายเลขเดือน"
		      End if
		    Else
		      chk = date.Mid(4,3)
		      if not(months.IndexOf(chk.ToText) = -1)then
		        mtemp = Str(months.IndexOf(chk.ToText)+1)
		        if mtemp.Len < 2 Then
		          mtemp = "0"+mtemp
		        End if
		        mresult = date.Mid(1,2)+mtemp+date.Mid(8,4)
		        Return mresult
		      End if
		      MsgBox "กรุณาใส่ข้อมูลให้ถูกต้อง"
		      Return ""
		    End If
		  End if
		  
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function gDateToString(selectDate As Date) As String
		  Dim dateString As String
		  Dim day As String
		  Dim month As String
		  if selectDate <> Nil then
		    if selectDate.Day.ToText.Length < 2 then
		      day = "0"+selectDate.Day.ToText
		      if selectDate.Month.ToText.Length < 2 then
		        month = "0"+selectDate.Month.ToText
		      end if
		      dateString = day+month+selectDate.Year.ToText
		    end if
		  end if
		  return dateString
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function gDBConnect() As Boolean
		  If Not App.DB.Connect Then
		    gLogError("ไม่สามารถฐานข้อมูลได้")
		    Return False
		  Else
		    Return True
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function gDBTransactionError() As Boolean
		  If App.DB.Error Then 
		    Utils.gLogError("DB Error: " + App.DB.ErrorMessage)
		    Return false
		  Else
		    // Utils.gLogError("Transaction สำเร็จ")
		    Return true
		  End If
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub gLogError(msg As String)
		  MsgBox msg
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function gRemoveSlashFromStringDate(dString As String) As String
		  return dString.ReplaceAll("/","")
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function gStringToDate(dateTxt As String, operator As String = "/") As Date
		  Dim newDate As New Date
		  
		  Dim dateTemp as String
		  dateTemp = gDateDisplayFormat(dateTxt)
		  
		  newDate.Day = val(dateTemp.Mid(1,2))
		  newDate.Month = val(dateTemp.Mid(3,2))
		  newDate.Year = val(dateTemp.Mid(5,4))
		  
		  return newDate
		  
		  
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function gValidateEmail(emailTxt As String) As Boolean
		  Dim result As Boolean
		  
		  Dim re As New RegEx
		  Dim rm As New RegExMatch
		  
		  If emailTxt = "" then
		    MsgBox "Input your Email"
		  else
		    re.SearchPattern = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
		    rm =re.Search(emailTxt)
		    
		    if rm = Nil Then
		      MsgBox "Email worng pattern."
		      result = False
		    Else
		      result = True
		    End
		  End
		  
		  return result
		  
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
