#tag Class
Protected Class App
Inherits Application
	#tag Event
		Sub Open()
		  Self.DB = new PostgreSQLDatabase
		  Self.DB.Host = "localhost"
		  Self.DB.Port = 5432
		  Self.DB.UserName = "postgres"
		  Self.DB.Password = "postgres"
		  Self.DB.DatabaseName = "ccc-onlineForm"
		  
		End Sub
	#tag EndEvent


	#tag Property, Flags = &h0
		DB As PostgreSQLDatabase
	#tag EndProperty


	#tag Constant, Name = kEditClear, Type = String, Dynamic = False, Default = \"&Delete", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"&Delete"
		#Tag Instance, Platform = Linux, Language = Default, Definition  = \"&Delete"
	#tag EndConstant

	#tag Constant, Name = kFileQuit, Type = String, Dynamic = False, Default = \"&Quit", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"E&xit"
	#tag EndConstant

	#tag Constant, Name = kFileQuitShortcut, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Mac OS, Language = Default, Definition  = \"Cmd+Q"
		#Tag Instance, Platform = Linux, Language = Default, Definition  = \"Ctrl+Q"
	#tag EndConstant

	#tag Constant, Name = kUnlockChilkat, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Anything for 30-day trial"
	#tag EndConstant

	#tag Constant, Name = kURLService, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"http://brainforsell.com:8080/"
	#tag EndConstant


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
